library(igraph)

get_phyla_connections <- function(ig, taxa){
  ig0e  <- get.edgelist(ig)
  phylumcombinations <- combn(names(table(taxa$Rank2)), 2)
  
  edgenumbers <- c()
  for ( x in 1:ncol(phylumcombinations)){
    p1 <- phylumcombinations[1,x]
    p2 <- phylumcombinations[2,x]
    p1_index <- row.names(taxa)[grep(p1, taxa$Rank2)]
    p2_index <- row.names(taxa)[grep(p2, taxa$Rank2)]
    edgenumbers <- c(edgenumbers, sum(ig0e[,1] %in% p1_index & ig0e[,2] %in% p2_index))
  }
  names(edgenumbers) <- sapply(1:ncol(phylumcombinations), function(x) paste(phylumcombinations[1,x], phylumcombinations[2,x]))
  return(edgenumbers)
}